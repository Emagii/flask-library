from flask import Blueprint, render_template, redirect, url_for, request, flash, session
from .forms import CreateBookForm, CreateLoantakerForm, CreateLoanForm, AdminLoginForm, CreateAdminForm
from flask import jsonify
from .models import Book, Loantaker, BookLoan
from . import db, querys
from functools import wraps
import datetime
import sqlite3

main = Blueprint('main',
                __name__,
                template_folder='templates/main')
admin = Blueprint('admin',
                __name__,
                template_folder='templates/admin')

@main.route('/')
@main.route('/index')
def index():
    date = datetime.datetime.today()
    books = Book.query.order_by(Book.title.asc()).all()

    loantakers = Loantaker.query.all()
    # Sql join query. Get book title matching loantaker id
    loans = db.session.query(Loantaker, BookLoan, Book)\
        .filter(Loantaker.id == BookLoan.loantaker_id)\
            .filter(BookLoan.book_id == Book.id)\
                .filter(Book.is_loaned == True)\
                    .filter(BookLoan.is_returned == False)

    return render_template('index.html',
                            books=books,
                            loantakers=loantakers,
                            loans=loans,
                            date=date)

@main.route('/create_book', methods=['GET', 'POST'])
@main.route('/create_book/', methods=['GET', 'POST'])
def create_book():
    form = CreateBookForm(request.form)
    
    if request.method == 'POST' and form.validate():
        new_book = Book(title=form.title.data,
                        author=form.author.data,
                        date_released=form.date_released.data)

        db.session.add(new_book)
        db.session.commit()

        return redirect(url_for('main.create_book'))
    else:
        return render_template('create_book.html', form=form)

@main.route('/update_book/<int:bookid>', methods=['GET', 'POST'])
def update_book(bookid):
    form = CreateBookForm(request.form)
    book = Book.query.filter(Book.id == bookid).first()
    
    if request.method == 'POST' and form.validate():
        book.title = form.title.data
        book.author = form.author.data
        book.date_released = form.date_released.data

        db.session.commit()

        return redirect(url_for('main.index'))
    else:
        return render_template('update_book.html', form=form, book=book)

@main.route('/create_loantaker', methods=['GET', 'POST'])
@main.route('/create_loantaker/', methods=['GET', 'POST'])
def create_loantaker():
    form = CreateLoantakerForm(request.form)
    
    if request.method == 'POST' and form.validate():
        new_loantaker = Loantaker(name=form.name.data,
                            lastname=form.lastname.data)

        db.session.add(new_loantaker)
        db.session.commit()

        return redirect(url_for('main.index'))
    else:
        return render_template('create_loantaker.html', form=form)

@main.route('/make_new_loan', methods=['GET', 'POST'])
@main.route('/make_new_loan/', methods=['GET', 'POST'])
def make_new_loan():
    form = CreateLoanForm(request.form)

    # Returns a list of all books matching
    book_choices = [(b.id, b.title) for b in Book.query.filter(Book.is_loaned == False).all()]
    form.book_id.choices = book_choices

    loantaker_choices = [(lt.id, f'{lt.name} {lt.lastname}' ) for lt in Loantaker.query.all()]
    form.loantaker_id.choices = loantaker_choices

    if request.method == 'POST' and form.validate():
        book_to_change = Book.query.filter_by(id = form.book_id.data).first()
        book_to_change.is_loaned = True
        new_loan = BookLoan(book_id=form.book_id.data,
                            loantaker_id=form.loantaker_id.data,
                            date_out=form.date_out.data,
                            date_in=form.date_in.data,
                            is_returned=False)

        db.session.add(new_loan)
        db.session.commit()

        return redirect(url_for('main.index'))
    else:
        return render_template('make_new_loan.html', form=form)

@main.route('/return_loan/<int:bookid>')
def return_loan(bookid):
    # Return book
    book_to_change = Book.query.filter(Book.id == bookid).first_or_404()
    book_to_change.is_loaned = False
    db.session.commit()

    # Set current time to bookreturn
    loan_to_change = BookLoan.query.filter(BookLoan.book_id == bookid)\
                                    .filter(BookLoan.is_returned == False).first_or_404()
    loan_to_change.date_in = datetime.datetime.now()
    db.session.commit()

    # Change to is returned
    loan_to_change.is_returned = True
    db.session.commit()

    return redirect(url_for('main.index'))
    
#
# ADMIN ROUTES
#

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash("You need to login first")
            return redirect(url_for('admin.admin_login'))

    return wrap

@admin.route('/admin/logout')
def admin_logout():
    
    session.pop('logged_in', None)
    session.pop('user', None)
    return redirect(url_for('admin.admin_index'))

@admin.route('/admin')
@admin.route('/admin/')
def admin_index():
    admin_list = querys.get_rows_all_records('users')
    
    return render_template('admin.html', admin_list=admin_list)


@admin.route('/admin/login', methods=['GET', 'POST'])
def admin_login():
    form = AdminLoginForm(request.form)

    if request.method == 'POST' and \
                form.validate() and \
                querys.check_if_user_exists(form.username.data) and \
                querys.validate_password_for_user(form.password.data, form.username.data):

        session['logged_in'] = True
        session['user'] = form.username.data

        return redirect(url_for('admin.admin_index'))

    else:
        # flash("Error logging in")
        return render_template('login.html', form=form)

@admin.route('/admin/signup', methods=['GET', 'POST'])
@login_required
def admin_signup():
    form = CreateAdminForm(request.form)

    if request.method == 'POST' and \
                form.validate() and \
                querys.check_if_user_exists(form.username.data) == False and \
                querys.create_new_user(name=form.username.data,
                                        email=form.email.data,
                                        password_as_string=form.password.data):
        flash(f'{form.username.data} created.')
        return redirect(url_for('admin.admin_index'))

    else:
        # flash("Error creating user")

        return render_template('signup.html', form=form)
