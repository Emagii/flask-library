import sqlite3
import os
import bcrypt

# Init DB and cursor
path = os.path.dirname(os.path.abspath(__file__))
db = os.path.join(path, 'db.sqlite3')
connection = sqlite3.connect(db, check_same_thread=False)
cursor = connection.cursor()

def update_row(database_name: str, table_name: str, column_name: str, value: int, whereid: str):

    base_command = ("UPDATE '{table_name}' SET '{column_name}' = '{value}' WHERE id is '{id}'")
    sql_command = base_command.format(table_name=table_name,
                                        column_name=column_name,
                                        value=value,
                                        id=whereid)

    cursor.execute(sql_command)
    connection.commit()

def get_rows(table_name: str, value: int, whereid: str) -> list:

    base_command = ("SELECT * FROM '{table_name}' WHERE '{whereid}' = '{value}'")
    sql_command = base_command.format(table_name= table_name,
                                        whereid= whereid,
                                        value= value)
    query_list = cursor.execute(sql_command).fetchall()
    return query_list

def get_rows_all_records(table_name: str) -> list:

    base_command = ("SELECT * FROM '{table_name}'")
    sql_command = base_command.format(table_name= table_name)
    query_list = cursor.execute(sql_command).fetchall()
    return query_list

def validate_password_for_user(password_as_string: str, name: str) -> bool:
    '''
    Encodes password string and checks db if usernames password hash match.
    '''

    password_encoded = bytes(password_as_string.encode())
    if bcrypt.checkpw(password_encoded, cursor.execute(
        """SELECT password FROM users WHERE name == ?""", (name, )).fetchone()[0]):
        return True
    else:
        return False

def check_if_user_exists(name: str) -> bool:
    '''
    Check if user exists. If no record returns None list
    '''
    if not cursor.execute('''SELECT name FROM users WHERE name ==?''', (name, )).fetchone():
        return False
    else:
        return True

def create_new_user(name: str, email: str, password_as_string: str):
    '''
    Creates user with values
    '''
    password_encoded = password_as_string.encode('utf-8')
    salt = bcrypt.gensalt()
    hashed_password = bcrypt.hashpw(password_encoded, salt)

    try:
        cursor.execute('''INSERT INTO users(name, email, password)
                VALUES(?, ?, ?)''', (name, email, hashed_password))
    except sqlite3.IntegrityError:
        print('User already exists')
    connection.commit()

def delete_user(name: str):
    '''
    Deletes user if it exists
    '''
    try:
        if check_if_user_exists(name):
            cursor.execute('''DELETE FROM users
                WHERE name == ?''', (name, ))
            connection.commit()
            return True
        else:
            print('User does not exist')
            return False
    except sqlite3.IntegrityError:
        print('User already exists')
    connection.commit()

