from . import db

class Loantaker(db.Model):
    __tablename__ = 'loantaker'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    lastname = db.Column(db.String(50))

    # one-to-many
    loans = db.relationship('BookLoan', backref='loantaker')

    def __repr__(self):
        return '<Loantaker %r>' % self.name

class Book(db.Model):
    __tablename__ = 'book'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    author = db.Column(db.String(50))
    date_released = db.Column(db.DateTime())
    is_loaned = db.Column(db.Boolean(), default=False)

    # one-to-one
    loan_id = db.relationship('BookLoan', backref='book_loaned', uselist=False)

    def __repr__(self):
        return '<Book %r>' % self.title

class BookLoan(db.Model):
    __tablename__ = 'bookloan'
    id = db.Column(db.Integer, primary_key=True)

    book_id = db.Column(db.Integer, db.ForeignKey('book.id'))
    loantaker_id = db.Column(db.Integer, db.ForeignKey('loantaker.id'))
    is_returned = db.Column(db.Boolean(), default=False)

    date_out = db.Column(db.DateTime())
    date_in = db.Column(db.DateTime())
