from wtforms import Form, BooleanField, StringField, validators, SelectField, PasswordField, SubmitField, IntegerField
from wtforms.fields.html5 import DateField


class CreateBookForm(Form):
    title            = StringField('Title', [validators.Regexp(r'^[\w. +-]+$'), 
                                            validators.Length(min=4, max=25),
                                            validators.DataRequired()])
    author           = StringField('Author', [validators.Length(min=6, max=35),
                                            validators.InputRequired()],
                                            description="test")
    date_released    = DateField('Date released', format='%Y-%m-%d')

class CreateLoantakerForm(Form):
    name            = StringField('Name', [validators.Length(min=4, max=25)])
    lastname        = StringField('Lastname', [validators.Length(min=4, max=25)])

class CreateLoanForm(Form):
    book_id            = SelectField(u'Book', coerce=int)
    loantaker_id       = SelectField(u'Loantaker', coerce=int)
    date_out           = DateField('Date loaned', format='%Y-%m-%d')
    date_in            = DateField('Date to returned', format='%Y-%m-%d')

class ReturnLoanForm(Form):
    book_id            = SelectField(u'Book', coerce=int)
    loantaker_id       = SelectField(u'Loantaker', coerce=int)
    date_in            = DateField('Date to returned', format='%Y-%m-%d')

class AdminLoginForm(Form):
    username            = StringField('Username', [validators.Regexp(r'^[\w. +-]+$'), 
                                            validators.Length(min=4, max=25),
                                            validators.DataRequired()])
    password            = PasswordField('Password', [validators.Regexp(r'^[\w. +-]+$'), 
                                            validators.Length(min=4, max=25),
                                            validators.DataRequired()])
    #submit              = SubmitField('Submit')

class CreateAdminForm(Form):
    username            = StringField('Username', [validators.Regexp(r'^[A-z0-9_-]{3,16}$'), 
                                            validators.Length(min=4, max=25),
                                            validators.DataRequired()])
    email            = StringField('Email', [validators.Email(),
                                            validators.DataRequired()])
    password            = PasswordField('Password', [validators.Length(min=4, max=25),
                                            validators.DataRequired(),
                                            validators.EqualTo('confirm', message='Passwords must match')])
    confirm            = PasswordField('Repeat Password')
    #submit              = SubmitField('Submit')
