from flask import Flask, Blueprint
from flask_sqlalchemy import SQLAlchemy
import os

db = SQLAlchemy()

def create_app():
    app = Flask(__name__)

    app.config['SECRET_KEY'] = 'Supersecret'
    app.secret_key = os.urandom(24)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite3'
    # The absolute path of the directory containing PDF files for users to download

    db.init_app(app)

    # import blueprints
    from .views import main, admin

    app.register_blueprint(main)
    app.register_blueprint(admin)

    return app