from ..models import Book
 
def test_new_book():
    """
    GIVEN a Book model
    WHEN a new Book is created
    THEN check the values
    """
    new_book = Book(title= 'The Lord of the rings',
                author= 'Tallefjanten')
    assert new_book.title == 'The Lord of the rings'
    assert new_book.author == 'Tallefjanten'
    assert not new_book.is_loaned
