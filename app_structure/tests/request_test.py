import pytest
import requests

@pytest.fixture
def create_request():
    '''
    Makes the request object
    '''
    response = requests.get(f'http://localhost:5000/')

    return response

def test_make_request_index(create_request):
    
    assert create_request.status_code == 200

